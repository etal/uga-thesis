# Compile a LaTeX manuscript
# Depends: texlive-latex-recommended, latex2rtf

finalname=talevich_eric_w_201308_phd.pdf
name=thesis
chaptex= introduction.tex methods.tex apikinome.tex ropk.tex conclusion.tex
xtra=abstract.tex acknowledgements.tex

# all: $(finalname)
all: $(name).pdf

clean:
	rm -vf *.aux *.bbl *.blg *.log *.lof *.lot

$(finalname): $(name).pdf
	cp $< $@

$(name).pdf: $(name).tex $(xtra) $(chaptex:.tex=.bbl)
	pdflatex $<
	pdflatex $<

$(chaptex:.tex=.bbl): %.bbl: %.aux $(name).bib
	bibtex $<

$(name).aux $(chaptex:.tex=.aux): %.aux: %.tex
	pdflatex $(name).tex

$(name).tgz: $(name).tex $(xtra) $(chaptex) $(name).bib Makefile
	tar -czvf $@ $^

