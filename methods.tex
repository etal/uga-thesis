\chapter{Methods for evolutionary analysis of protein families}
% --------------------------------------------------------------------------
% Enough details should be provided of materials and methods so that someone
% expert in the field could reproduce the work—preferably without burrowing
% through a long chain of references Wherever possible, all data and analyses
% should be provided in the thesis, so that their veracity can be checked by the
% examiner. Where this might interrupt the narrative flow of the thesis, these
% should be presented in an appendix or, if datasets are large, they should be
% provided on a DVD.
%
% Each line of experimental work should be given its own results chapter, with a
% brief introduction, a results section and a discussion section for each of the
% chapters. Methods specific to a given chapter might also feature in a dedicated
% methods section, although a Methods chapter dedicated to commonly used methods
% could be included.
%
% The introduction to each results chapter should describe and justify the
% rationale for the programme of experimental work described in that chapter.
% Careful consideration should be given to what appears in the Introduction to the
% thesis as a whole and what should be reserved for the introduction to a specific
% results chapter, so as to present the most logical flow of ideas, while avoiding
% repetition. The narrative describing the progress of experiments and
% interpretation of data should deliver a simple and coherent description of the
% work undertaken. Long verbose descriptions of results should be avoided—instead,
% material should be summarised effectively in tables, figures or in crisp and
% lively prose.
%
% Students should avoid over-interpretation of results --- the student should
% distinguish between situations where the data are merely consistent with a given
% interpretation and where they actually provide convincing evidence for it.
%
% Results from experimental controls should be presented whenever relevant. The
% number of replicates carried out for each experiment should be described and the
% reproducibility of techniques discussed. The potential for technical errors
% should be discussed together with the steps that the student took—or might have
% taken—to avoid them.
%
% Where, for historical or financial reasons, the student has not adopted the most
% direct or most informative approach to a problem, this should be explained
% carefully. The student should attempt to evaluate the importance and
% shortcomings of each piece of work, in terms of how far it tells us new things
% about the world, represents an achievement in method development and/or provides
% resources for subsequent work.
% --------------------------------------------------------------------------

% "Development of new methods"
% ``Development and application of computational methods for analysis of protein
% families.''
% -> describe the workflow for characterizing lineage-specific divergences

In this chapter I review the methods used to characterize eukaryotic kinomes
and lineage-specific kinase families and to place the results in the context of
molecular and organismal function.
Since apicomplexans are divergent from model organisms and
are often difficult to study in the lab, computational biology has been
important in obtaining preliminary insights into the functions of these species.
A common pattern is to use computational methods initially to develop
hypotheses, and then test the hypotheses experimentally. More recently,
high-throughput experiments on \textit{P.~falciparum} and \textit{T.~gondii}
have provided data that can be mined and integrated; again, the insights
obtained for these two species can also be applied to apicomplexan relatives
with the appropriate \textit{in silico} methods.


\section{Data resources}

    \subsection{Pathogen-specific databases}

    To date, the genomes of 15 apicomplexan species have been fully sequenced
    and at least partially annotated: \textit{Babesia bovis},
    \textit{Cryptosporidium hominis}, \textit{C.~muris}, \textit{C.~parvum},
    \textit{Eimeria tenella}, \textit{Neospora caninum}, \textit{Plasmodium
    falciparum}, \textit{P.~berghei}, \textit{P.~chabaudi},
    \textit{P.~knowlesi}, \textit{P.~vivax}, \textit{P.~yoelii},
    \textit{Theileria annulata}, \textit{T.~parva}, and \textit{Toxoplasma
    gondii}.

    GeneDB \cite{Logan-Klumpler2012}, under the umbrella of Wellcome Trust
    Sanger Institute (WTSI) Pathogen Genomics, provides a community resource for
    collecting and accessing the data produced by these projects, including
    whole-genome sequences, EST data, and community annotation projects.
    Several other genomes were produced by the J.\ Craig Venter Institute (JCVI).
    In addition, sequences have been deposited directly in the universal
    sequence repositories (NCBI GenBank, EBI ENA, DDBJ).

    A family of websites provides an efficient and user-friendly entry point to
    these and many other large-scale data for specific apicomplexan species:
    PlasmoDB (\textit{Plasmodium} spp.), ToxoDB
    (\textit{Toxoplasma gondii} strains, \textit{Neospora caninum},
    \textit{Eimeria tenella}, and a planned \textit{Sarcocystis neurona}),
    CryptoDB (\textit{Cryptosporidium} spp.), and the recently added PiroplasmDB
    (\textit{Theileria} spp.\ and \textit{Babesia bovis}).
    All of these are united behind a
    portal for eukaryotic pathogens, EuPathDB, formerly ApiDB
    \cite{Aurrecoechea2010,Aurrecoechea2012}.
    EuPathDB also aggregates functional genomics data such as mRNA expression
    from microarray and RNA-Seq experiments, and proteomics and
    phospho-proteomics data from mass spectrometry. Datasets are typically taken
    from published articles and uploaded by the authors with the assistance of
    EuPathDB staff. The website provides a useful ``strategies'' interface in
    which different queries can be combined to filter data sets for specific
    properties; results can be browsed online, saved or downloaded in batch.

    \subsubsection{Protein structures}

    The Protein Data Bank (PDB) is well established as the canonical repository
    for protein structural data.
    The Structural Genomics Consortium (SGC) has taken on the challenge of
    solving neglected parts of the protein structural space on a per-family
    basis with specific attention to protein kinases \cite{Gileadi2007}. The
    University of Toronto branch of the SGC has in particular
    focused on solving the structures of kinases in apicomplexans and other
    pathogenic protozoa, and since 2004 have deposited many novel structures of
    apicomplexan kinases, including both eukaryote-conserved and
    lineage-specific subfamilies, in PDB for public use. Specific findings from
    this work on kinases in \textit{P.~falciparum}, \textit{T.~gondii} and
    \textit{C.~parvum} have been described, with particular focus on the
    calcium-dependent protein kinase
    family \cite{Wernimont2010,Wernimont2010b,Artz2011}; however, the SGC has
    also released a number of structures ahead of any manuscript publication,
    and these can also be accessed from PDB.

    \subsection{Protein kinase classifications and profiles}

    The eukaryotic protein kinase (ePK) superfamily is hierarchically classified
    into eight major groups: (AGC), calcium- and calmodulin-dependent kinases
    (CAMK), casein kinase 1 and its relatives (CK1), relatives of the cell-cycle
    control kinases CDK, MAPK, GSK3 and CLK (CMGC), receptor guanylate cyclase
    (RGC), a group containing the yeast protein Sterile11 (STE), tyrosine
    kinases (TK), and tyrosine-kinase-like kinases (TKL) \cite{Hanks1995}.
    Several families and subfamilies have been defined within each group, as
    well as a number of families that do not share the
    characteristics of any of the major groups (known as the ``Other'' group).

    Families that do not share identifiable sequence homology to the ``typical''
    ePKs are designated atypical protein kinases (aPK); some of these, such as
    Alpha, PIKK and RIO, nonetheless share the overall bilobate structural fold
    of ePKs, and are known as protein kinase-like kinases (PKL), while others
    such as pyruvate dehydrogenase kinase (PDHK) and nucleoside diphosphate
    kinase (NDK) adopt entirely different folds and appear to have independently
    evolved the functionality of protein phosphorylation \cite{Manning2002a}.

    The online database KinBase provides the classification and protein
    sequences of the kinomes of many model organisms, along with the
    characteristic domain architectures, descriptions, phyletic profiles, and
    sequence alignments of each of the recognized families. These family
    designations correspond to the first comparative analysis of human kinases
    and the model organisms yeast, fruit fly and nematode \cite{Manning2002b},
    and this terminology has been broadly adopted by other annotators.


    Other databases have emerged to associate additional types of information
    with kinases. KinG \cite{Krupa2004}, a database of ``kinases in
    genomes,'' provides classifications based on sequence analysis methods, with
    specific attention to the domain architectures characteristic of each kinase
    family.
    Kinomer \cite{Martin2009} provides automated kinome annotations for a
    broader range of model organisms, including \textit{Plasmodium falciparum}.
    In addition, Kinomer provides a service to classify user-supplied sequences
    using a group-specific HMM profile set, following the same hierarchical
    classification scheme, though only to the level of major groups and
    selected atypical families.

    The Protein Kinase Resource \cite{Niedner2006} is an integrated resource
    for studying protein kinase sequence and structure, providing an
    interactive visualization of protein kinase structures from PDB alongside
    with relevant information from UniProt. A focused ontology for protein
    kinases, ProKinO, has also been developed to provide a controlled
    vocabulary of terms and relationships unifying kinase sequence, structure
    and functional information \cite{Gosal2011}.



\section{Bioinformatic methods for protein subfamily classification}

  Given the abundance of information available to be mined for new insights,
  data-driven approaches to the characterization of apicomplexan kinomes are
  appealing.

    \subsection{Sequence similarity}
    % JCK: Don't confuse detection of similarity e.g. BLAST with proof of
    % evol. ancestry. Similarity has many causes, one of which is homology.
    % Homology = shared evol. ancestry, e.g. descent from common ancestor

    Many of the kinases in the annotated apicomplexan genomes were assigned
    functional descriptions based on close matches to homologs, typically based on
    BLAST search and functional domains identified by Pfam's HMM profile search. The
    annotation of parasite genomes is often organized through GeneDB, an online
    community resource provided by the Wellcome Trust Sanger Institute Pathogen
    Genomics group that combines these automated results with in-progress
    annotations from curators \cite{Logan-Klumpler2012}.

    The detail of kinase annotations can be improved with group- or family-specific
    HMM profiles. Kinomer uses such profiles based on the previously annotated
    kinomes of many species to provide accurate group-level kinase classifications,
    as well as improve the overall sensitivity of searches over a generic protein
    kinase profile \cite{Martin2009}. Kinannote is another automated
    kinase classifier based on family-specific sequence profiles; an early version
    of it was applied to the kinome of the mushroom \textit{Coprinopsis cinerea}
    \cite{Stajich2010}.
    An alternative to HMM profiles is position-specific scoring matrices, as
    implemented in PSI-BLAST \cite{Altschul1997} and the related tool MAPGAPS
    \cite{Neuwald2009}.

    Domain architectures, as determined by Pfam or similar services, can provide
    clues to a kinase's classification. For example, members of the cGMP-dependent
    protein kinase (PKG) family characteristically contain a series of
    cyclic-nucleotide-binding domains on the same protein sequence as the kinase
    domain, and similarly, calcium-dependent protein kinase (CDPK) is associated
    with four calcium-binding ``EF-hand'' domains.

    \subsection{Evolution-based methods}
    % JCK: e.g. detection of homology

    Orthology with a characterized kinase in another species serves as a strong
    signal for transferring functional annotations. The most literal approach to
    determine orthologous groups of genes is to infer a gene tree using an
    appropriate phylogenetic model, and compare the topology of the resulting gene
    tree to that of the accepted species tree. Genes which have been replicated
    through speciation rather than gene duplication are considered orthologs. Since
    the species relationships between all of the fully sequenced apicomplexans have
    been established \cite{Kuo2008a}, this approach is feasible with
    individual apicomplexan kinomes or with specific kinase groups families shared
    across multiple apicomplexan species. At the cost of some accuracy, orthologous
    groups can also be inferred in large data sets through a combination of
    reciprocal best BLAST hits across species and distance-based clustering, as
    implemented together in the program OrthoMCL \cite{Li2003}. The
    orthology relationships determined across a large number of complete genomes,
    including most of the apicomplexan species discussed here, are available through
    OrthoMCL-DB \cite{Chen2006a}.

    Within a kinase group, family, subfamily or ortholog group, it is then useful to
    examine patterns of conservation and selection in aligned sequences in order to
    identify possible sites of adaptation, subfunctionalization and
    neofunctionalization. Peixoto et al. \cite{Peixoto2010} and Reese et
    al. \cite{Reese2011a} used the ratio of nonsynonomous and synonymous
    SNPs in aligned codons to identify regions and sites of positive selection in
    rhoptry kinases. Talevich et al. \cite{Talevich2011} used binomial tests of
    amino acid frequencies and a Bayesian pattern partitioning procedure, as
    implemented in CHAIN \cite{Neuwald2007}, to identify instances of
    change/gain of function as well as find taxa that share the same selective
    constraints.


    % --------------------------------------------------------------------------

    There are several ways misclassification of kinases can occur.
    When classification is based on similarity to a single sequence, as with
    BLAST, it is possible to find highly significant matches to paralogous
    proteins; this risk is greater in a highly expanded protein superfamily such
    as the protein kinases, and can be compounded by the bias in sequence
    representation in databases toward model organisms and the specific gene
    subfamilies their genomes contain.
    Curated sequence profiles constructed from diverse sequence sets such as
    the UniRef50 database, can reduce the bias in taxon sampling across all
    eukaryotes and thus assists classification of kinase sequences which have
    diverged from those of model organisms.
    However, the problem remains that an orphan sequence will be assigned to the
    best-matching query profile even if it represents a paralogous subfamily
    which is not represented in the profile set.
    In the following studies I conducted, I addressed this problem by
    constructing the ePK sequence profile database using a hierarchical scheme
    which allows an atypical sequence to be classified according to a broader
    group (such as CMGC) if it does not show substantial similarity to a more
    specific family.
    This reduces the likelihood of incorrect assignment of novel kinase
    sequences to an established subfamily, and facilitates the identification of
    divergent ePKs that may warrant deeper investigation, such as the unique
    MAPK subfamily found in all apicomplexans.
    In addition, I have continually updated the sequence profiles in accordance
    with published literature to include newly characterized sequences and ePK
    families.


    %  Ortholog determination; subfamily validity
    %    % problem
    %    these kinases are diverse, w/ indels;
    %    kinase domain has a limited number of conserved columns
    %    -> gene tree is not impressively well-resolved
    %    % soln:
    %    confirm orthologs with OrthoMCL-DB


% --------------------------------------------------------------------------

\section{Methods for finding divergent clades}

  After a gene duplication event produces two copies of the same gene in a
  single genome, one copy may evolve to gain or lose functions relative to the
  other copy which is presumed to retain the gene's original function
  \cite{Zhang2003,Altenhoff2012}.
  In this section I review methods to find distinct protein clades that may have
  emerged through a process similar to this,
  and to study the fate of this
  divergent copy, specifically to identify and characterize novel functions or
  mechanisms it may have evolved.

  \subsection{Phylogenetic analysis}

  % definition of homology/Orthology : fitch1970

  Orthologous genes between species can be predicted by comparing the gene tree
  and species tree to infer duplication events (Figure~\ref{fig:treesubfam}).
  % One method for this is RIO, Reliable Inference of Orthologs \cite{Zmasek2002}.
  Note that orthology among multi-gene families is determined with respect to
  some common ancestral point which is considered the origin of the family of
  interest.
  For example, CDPK subfamilies are paralogous with respect to the root or
  origin of the CDPK family (assuming a single origin), but can be considered
  orthologs (or in-paralogs) with respect to a broader grouping such as the CAMK
  kinase group or the ePK superfamily.
  Since multiple levels of comparision possible, the point of reference must be
  decided to address the research problem in question.

  % \begin{figure}[htp]
  \begin{FPfigure}
    \center{\includegraphics[width=0.9\textwidth]{fig-intro/cdpk-tree-rooted.pdf}}
    \caption[Maximum likelihood gene tree of the calcium-dependent protein
    kinase family]{%
    Maximum likelihood gene tree of the calcium-dependent protein kinase family
    (CDPK), inferred from conserved amino acid sites of the protein kinase
    domain using FastTree 2 \cite{Price2010}.
    Colors indicate an apicomplexan-specific subfamily of interest (red), other
    apicomplexan CDPKs (purple), and plant CDPKs (green).}
    \label{fig:treesubfam}
  \end{FPfigure}


    \subsubsection{Multiple sequence alignment}

    The accuracy of an inferred phylogenetic tree depends critically on the
    accuracy of the input character alignment.
    % The pairwise sequence alignment problem can be extended to multiple
    % nucleotide or protein sequences.
    Because the optimal algorithmic solution quickly becomes intractable using
    dynamic programming methods \cite{Lipman1989,Wang1994}, a variety of methods
    have been devised to obtain approximate solutions to the problem
    \cite{Thompson1994,Notredame2000,Edgar2004,Neuwald2004,Katoh2005,Kemena2009}.
    These \textit{de novo} multiple sequence alignment methods are more likely
    to produce incorrect alignments if the input sequences are too few, too
    numerous, or too divergent.
    In those cases it is preferable to use structure-based or profile-based
    methods for sequence alignment.

    HMMer can align many sequence to a single profile using the ``hmmalign''
    command \cite{Eddy2011}.  MAPGAPs \cite{Neuwald2009} uses a set of several
    profiles representing subfamilies of a structurally related protein
    superfamily, each of which is aligned to the others. Thus, if the profile
    alignment has been constructed accurately (using 3D alignments of solved
    protein structures, for example), sequences belonging to divergent
    subfamilies can be aligned to each other with similar accuracy, potentially
    improving upon the alignments generated by the single-profile approach used
    in HMMer.


  \subsection{Orthology prediction methods and databases}

  Public databases that group genes into ortholog groups have been available for
  many years.
  The problem of inferring ortholog groups at the genome scale across many taxa
  has been approached with automated methods.
  One approach which has been implemented by several teams is to group sequences
  by reciprocal best BLAST hits: Perform an all-versus-all BLAST search to
  identify each protein's closest hit in each other genome; those hits for which
  the query is likewise the best hit performing the search the other direction
  are possible orthologs.
  This is implemented in the programs TribeMCL \cite{Enright2002} and OrthoMCL
  \cite{Li2003}, and a database of OrthoMCL results run on many whole genomes is
  available as OrthoMCL-DB \cite{Chen2006a}.
  Other teams have used profile-based approaches, including PHOG
  \cite{Datta2009}, based on the predictions of the programs FlowerPower and
  SCI-PHY \cite{Brown2007}.
  KinBase \cite{Manning2002b} also maps orthologous genes between model
  organisms and several other species, though it is limited to genes that code
  for protein kinases.


\subsection{Sequence profile construction and curation for protein families}

The program Fammer (\url{http://github.com/etal/fammer}) partially automates the
process of sequence profile construction and curation for protein families and
subfamilies.
Designed for use with MAPGAPS and HMMer 3.0, Fammer creates a tree of profiles,
with higher-level alignments representing broader groups and the top-level
alignment representing the entire protein superfamily of interest.
Several sub-commands are available to:
build HMMer 3.0 and MAPGAPS profiles from a directory tree;
scan protein sequences to assign hits to best-matching HMM profiles;
add new sequences to the profile tree by searching a target database;
refine sequence profiles using leave-one-out validation;
and cluster a large family-level sequence alignment into phylogenetically
supported subfamilies.
After profile construction, the program can be applied to scan a proteome to
automatically identify and classify the kinome with high accuracy.


\section{Statistical comparison of clades (sub-alignments)}
% ENH - take some of the blast-cap & cladecompare notes on column comparision

The selective pressures or constraints on a site are partly revealed through
the mutation rates at the site among orthologous proteins.
Negative selection is observed as conservation of a site relative to the average
mutation rate, while faster mutation rates at a site suggest neutral or positive
selection.
The effects of selection on individual sites in a protein can be quantified
using an alignment of orthologous gene sequences.
One method to distinguish between the two involves comparing the rates of
nonsynonymous mutations (assumed to produce potentially functional changes which
are subject to selection) to synonomous mutations (assumed to be under
approximately neutral selective pressure); if this ratio (dN/dS) is greater than
1, this indicates that the site or gene in question is under positive selection.

At the amino acid level, conservation of a site relative to the other sites in
the alignment indicates selective pressure. By comparing two or more sets of
sequences, as is done by the programs CHAIN \cite{Neuwald2007} and
CladeCompare (\url{http://github.com/etal/cladecompare}), statistical tests can
identify the sites that show the greatest difference in selective pressures, or
different in equilibrium states of the sites, between the compared sets.

  % related: rate4site, intrepid
  % raser, diverge


  \subsection{Statistical comparison of alignments \& models of site divergence}

    \subsubsection{Ball-in-urn model}

    The urn model poses the question: If we sample $N$ sequences and get $k$
    residues of consensus type, what are the odds given the background frequency
    $p$?  % pull N balls out of a big urn, get k red balls
    This follows the binomial distribution:

    \[
    P(x|k,N,p) = \sum_{i=k}^{N} \binom{N}{i} p^i (1-p)^{N-i}
    \]

    This test is implemented in CHAIN and CladeCompare.
    In CHAIN, this test statistic for a sampling of ``pattern'' sites is used as
    the optimization criterion for a Markov Chain Monte Carlo algorithm which
    attempts to assign sequences to ``foreground'' and ``background'' sets in
    order to maximize the contrast between them at the selected pattern sites
    \cite{Neuwald2003}.


    \subsubsection{G-test}

    The G-test is a goodness-of-fit test between residue frequencies observed in
    foreground versus those expected based on the background composition
    \cite{Dunning1993}.
    The test is conceptually similar to the chi-squared test, but is more robust
    on smaller samples, including sets with counts of 0.

    % - Contingency table of all residue frequencies between foreground and
    % background columns (like chi-square)
        % - figure: the contingency table for one example column

    \[
    G = 2 \sum_{i \in a.a.} O_i \ln \frac{O_i}{E_i}
    \]

    The test statistic follows the chi-squared distribution with 19 degrees of
    freedom, for the 20 amino acids minus one:

    \[
    G \sim \chi^2_{19}
    \]

    This model is implemented in CladeCompare. In addition to gain-of-function
    sites, this test can also reveal loss-of-function sites when comparing a
    subfamily to its broader family, unlike the urn model.

    %     - test every aligned column and apply multiple testing correction

    %   - Caveats:
    %     - No evolutionary model (yet)

    The program CladeCompare implements several fast statistical tests for
    finding diagnostic sites between two given clades, using a modular
    ``strategies'' framework for modeling different types of site divergence.
    Unlike CHAIN, there is no Bayesian resampling of the two given sets.
    Instead, the user must first identify the clades of interest, typically
    through phylogenetic methods.


    \subsection{Sequence weighting}

    Both statistical tests described above assume observations are independent.
    However, because the sequences in each clade are phylogenetically related,
    they are by definition not independent. Thus, a weighting scheme must be
    applied to the sequences to correct for the phylogenetic relatedness between
    sequences \cite{Felsenstein1985}.

    In both CHAIN \cite{Neuwald2003} and CladeCompare, the aligned sequences in
    each set are weighted according to the Henikoff heuristic
    \cite{Henikoff1994}, following the same approach as and PSI-BLAST
    \cite{Altschul1997}.

    In brief, for each column of the alignment, the algorithm counts the number
    of rows and the number of occurrences of each amino acid type among all rows
    within the column.
    A per-column weight of 1 is first divided by the number of distinct residue
    types, then for each type, divided again by the number of sequences sharing
    that residue type and assigned to each corresponding sequence.
    (For example, in a column composed of the residues [`A', `V', `V', `V'],
    there are two amino acid types `A' and `V'; since `A' occurs once, the first
    sequence receives a weight of $\frac{1}{2}$, and since `V' occurs three
    times, the remaining weight is divided evenly between then, so each receives
    a weight of $\frac{1}{6}$.)
    These site weights are summed across all columns to compute weights for each
    sequence, and typically normalized for further calculations.

    In CladeCompare, the effective overall number of sequences is estimated
    using another heuristic which, to our knowledge, has not been applied
    previously.
    The purpose of this step is to scale the weights obtained above according to
    the number of distinct residues that would be observed in an equally sized
    set of random, independent sequences.
    This value is estimated as the expected number of distinct residues to be
    observed in a column of given height (following a multinomial distribution,
    or for speed, pre-computed for a given number of rows by simulation),
    multiplied by the number of aligned columns, skipping columns which are
    mostly gaps.
    This value, the total number of ``independent'' residues, is then divided by
    the number of sequences in the alignment to obtain an estimate of the
    average sequence length.
    Finally, for each sequence, the sum of per-site weights is divided by the
    average sequence length. The sum of these final weights is the effective
    number of independent sequences in the alignment.
    This method is implemented in the supporting library BioFrills
    (\url{http://github.com/etal/biofrills}).

      % for each column, tot_nres += min(nrows, 20)
      % -> tot_nres = min(#rows, 20) * #non-gap-cols  -- tot. indep. res.
      % avg_seq_len = tot_nres / len(aln)   -- avg. col. depth?
      % each seq weight = sum_of_sites / avg. col. depth


    % \subsection{Visualizations}

    % \subsubsection{Sequence logos}

    % A sequence logo represents conservation of characters at each site in an
    % alignment. Specifically, the total height of each letter ``stack''
    % represents the different in entropy between that observed in the alignment
    % and the theoretical maximum in which all possible characters occur with
    % equal frequency. A widely used implemention is Weblogo  \cite{Crooks2004}.

    % In CladeCompare, contrast between foreground and background is visualized as
    % a pair of logos, one each for the foreground and background alignments, and
    % highlighted bars connecting the two at each site showing significant
    % contrast in composition according to the chosen statistical test.


    % \subsubsection{Heat map}

    % CladeCompare generates a ``heat map'' of site divergence (from p-values)
    % in the form of an HTML document which can be viewed in a web browser.



\section{Structural mapping}

The placement of identified sites of interest into a structural context on a
solved protein crystal structure can allow mechanistic interpretation of the
features that are unique to the protein subfamily being investigated.

We use CladeCompare and other in-house scripts to map sites of significant
constrast onto PDB structures according to a multiple sequence alignment that
includes the sequences of the foreground and background sets, as well as the
primary sequence of the protein crystal structure itself.
The alignment itself is generated using MAPGAPS 1.0 \cite{Neuwald2009} or HMMer
3.0 \cite{Eddy2011}; in either case, the alignment is made in reference to a
previously constructed profile representing a fixed set of ``consensus'' or
``match'' columns, as well as sequence-specific insertions and deletions.  Thus,
if the foreground, background and PDB sequences are all aligned with the same
MAPGAPS or HMMer profile, the equivalent (i.e.\ homologous) ``consensus'' columns
can be easily identified in all three sets. This property allows us to
automatically map significant alignment sites to the structure using the aligned
amino acid sequence of the protein structure.
CladeCompare uses this information to generate a script in the syntax of the
molecular viewer program PyMOL \cite{Pymol2011} to visualize selected sites on a
given PDB structure.

\bibliographystyle{natbib}
\bibliography{thesis}
